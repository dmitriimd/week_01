package ru.edu.lesson1;

import junit.framework.TestCase;
import org.junit.Test;

public class HobbyTest extends TestCase {

    private Hobby hobby = new Hobby("1", "Sport", "Athletics");

    @Test
    public void testHobbyMethods() {
        assertEquals("1", hobby.getId());
        assertEquals("Sport", hobby.getName());
        assertEquals("Athletics", hobby.getDescription());
        assertEquals("Hobby{id='1', name='Sport', " +
                "description='Athletics'}", hobby.toString());
    }

}