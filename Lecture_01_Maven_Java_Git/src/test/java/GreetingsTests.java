import org.junit.Assert;
import org.junit.Test;
import ru.edu.lesson1.GreetingImpl;
import ru.edu.lesson1.Hobby;
import ru.edu.lesson1.IGreeting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

public class GreetingsTests {

    private IGreeting greeting = new GreetingImpl();
    @Test
    public void size_Test() {

        Assert.assertNotNull(greeting);

        assertEquals("Dmitrii", greeting.getFirstName());
        assertEquals("Mikhailovich", greeting.getSecondName());
        assertEquals("Dobrokhotov", greeting.getLastName());
        assertNotNull(greeting.getHobbies());
        assertEquals("https://DmitriiMD@bitbucket.org/dmitriimd/week_01.git",
                greeting.getBitbucketUrl());
        assertEquals("1234567890", greeting.getPhone());
        assertEquals("JavaDeveloperJunior", greeting.getCourseExpectation());
        assertEquals("EduUniversity", greeting.getEducationInfo());
        assertEquals("GreetingImpl{dmitrii='Dmitrii', mikhailovich='Mikhailovich', dobrokhotov='Dobrokhotov'}", greeting.toString());

        // check your methods
    }
}
