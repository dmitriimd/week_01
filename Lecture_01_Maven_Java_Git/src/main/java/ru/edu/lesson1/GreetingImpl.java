package ru.edu.lesson1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GreetingImpl implements IGreeting{

    private final String dmitrii = "Dmitrii";
    private final String mikhailovich = "Mikhailovich";
    private final String dobrokhotov = "Dobrokhotov";
    private List<Hobby> hobbies = new ArrayList<>();

    public GreetingImpl() {
        hobbies.add(new Hobby("1", "Sport", "Athletics"));
        hobbies.add(new Hobby("2", "Sport", "Swimming"));
        hobbies.add(new Hobby("3", "Programming", "Java"));
    }

    /**
     * Get first name.
     */
    @Override
    public String getFirstName() {
        return dmitrii;
    }

    /**
     * Get second name
     */
    @Override
    public String getSecondName() {
        return mikhailovich;
    }

    /**
     * Get last name.
     */
    @Override
    public String getLastName() {
        return dobrokhotov;
    }

    /**
     * Get hobbies.
     */
    @Override
    public Collection<Hobby> getHobbies() {
        return hobbies;
    }

    /**
     * Get bitbucket url to your repo.
     */
    @Override
    public String getBitbucketUrl() {
        return "https://DmitriiMD@bitbucket.org/dmitriimd/week_01.git";
    }

    /**
     * Get phone number.
     */
    @Override
    public String getPhone() {
        return "1234567890";
    }

    /**
     * Your expectations about course.
     */
    @Override
    public String getCourseExpectation() {
        return "JavaDeveloperJunior";
    }

    /**
     * Print your university and faculty here.
     */
    @Override
    public String getEducationInfo() {
        return "EduUniversity";
    }

    @Override
    public String toString() {
        return "GreetingImpl{" +
                "dmitrii='" + dmitrii + '\'' +
                ", mikhailovich='" + mikhailovich + '\'' +
                ", dobrokhotov='" + dobrokhotov + '\'' + '}';
    }
}
