package ru.edu.lesson1;

public class Hobby {

    private final String id;
    private final String name;
    private String description;

    public Hobby(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Hobby(String id, String name, String description) {
        this (id, name);
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
