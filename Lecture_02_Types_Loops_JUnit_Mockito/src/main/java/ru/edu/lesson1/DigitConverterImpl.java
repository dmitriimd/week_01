package ru.edu.lesson1;

public class DigitConverterImpl implements DigitConverterHard {
    /**
     * Преобразование целого десятичного числа в другую систему счисления
     *
     * @param digit - число
     * @param radix - основание системы счисления
     * @throws IllegalArgumentException если digit отрицательное число
     * @throws IllegalArgumentException если radix меньше единицы
     */
    @Override
    public String convert(int digit, int radix) {
        if (digit == 0) return "0";
        if (digit < 0) throw new IllegalArgumentException("digit отрицательное число");
        if (radix <= 1 ) throw new IllegalArgumentException("radix меньше-равно единицы");

        int tmp = digit;
        String res = "";
        while (tmp > 0) {
            res = (tmp % radix) + res;
            tmp = tmp / radix;
        }
        return res;
    }

    /**
     * Преобразование вещественного десятичного числа в другую систему счисления
     *
     * @param digit     - число
     * @param radix     - основание системы счисления
     * @param precision - точность, сколько знаков после '.'
     * @throws IllegalArgumentException если digit отрицательное число
     * @throws IllegalArgumentException если radix меньше единицы
     */
    @Override
    public String convert(double digit, int radix, int precision) {

        String entire = convert((int) digit, radix);

        double digit_fraction = digit - (int) digit; // Выделение дробной части
        String fraction = "";

        for (int i = 0; i < precision; i++) {
            digit_fraction *= radix;
            // выделяем целую часть числа, для формирования строки дроби
            fraction += String.valueOf((int) digit_fraction);
            if (digit_fraction >= 1) digit_fraction = digit_fraction - (int) digit_fraction + 0.000000000001;
        }
        return entire + "." + fraction;
    }
}
